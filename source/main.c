#include "draw.h"
#include "libc.h"
#include "utils.h"
#include "3dstypes.h"
#include "FS.h"

extern void *arm11_start;
extern void *arm11_end;
#define ARM_BRANCH(cur,dst) ((0b1110<<28) | (0b101<<25) | (0b0<<24) | ((dst-(cur+8))>>2))

#define STAGE1_ADDR       ((volatile void*)0x1FFF4B40)
#define MAGIC_ADDR        ((volatile void*)0x1FFFFFF8)
#define MAGIC_INT         (*(volatile int*)MAGIC_ADDR)
#define ARM11_EXCVEC_ADDR ((volatile void*)0x1FFF4000)
#define FCRAM_PA          ((volatile void *)0x20000000)
#define FCRAM_SIZE        (0x08000000)
#define LINUX_BASE_ADDR   (FCRAM_PA)

#define ATAGADDR   (LINUX_BASE_ADDR + 0x100)
#define ZRELADDR   (LINUX_BASE_ADDR + 0x8000)
#define INITRDADDR (LINUX_BASE_ADDR + 0x800000)

#define LINUXIMAGE_FILENAME "/dslinux_3ds.bin"

void pwn_ARM11()
{
	disable_IRQ();
	disable_FIQ();
	disable_MPU();
	
	unsigned int excvec_backup[8];
	
	//Backup ARM11 exception vectors
	memcpy(excvec_backup, (void*)ARM11_EXCVEC_ADDR, sizeof(excvec_backup));
	
	//Copy ARM11 payload
	memcpy((void*)STAGE1_ADDR, &arm11_start, (u32)&arm11_end - (u32)&arm11_start);
	
	//Set magic number to 0...
	MAGIC_INT = 0;
	
	//Overwrite ARM11 exception vectors with a jump to our payload
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 0)  = ARM_BRANCH(ARM11_EXCVEC_ADDR+0,  STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 4)  = ARM_BRANCH(ARM11_EXCVEC_ADDR+4,  STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 8)  = ARM_BRANCH(ARM11_EXCVEC_ADDR+8,  STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 12) = ARM_BRANCH(ARM11_EXCVEC_ADDR+12, STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 16) = ARM_BRANCH(ARM11_EXCVEC_ADDR+16, STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 20) = ARM_BRANCH(ARM11_EXCVEC_ADDR+20, STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 24) = ARM_BRANCH(ARM11_EXCVEC_ADDR+24, STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 28) = ARM_BRANCH(ARM11_EXCVEC_ADDR+28, STAGE1_ADDR);
	
	//Disabling the MPU also disables the caches,
	//so there's no need to flush the caches
	while (MAGIC_INT == 0) ;
	
	//Restore ARM11 exception vectors
	memcpy((void*)ARM11_EXCVEC_ADDR, excvec_backup, sizeof(excvec_backup));
	
	enable_MPU();
	enable_FIQ();
	enable_IRQ();
}

void map_VRAM_MPU()
{
	disable_IRQ();
	disable_FIQ();
	disable_MPU();
	asm volatile (
		//Drain write buffer
		"mcr p15, 0, r0, c7, c10, 4\n"
	
		//Map VRAM to region 7
		//Region base: 0x18000000
		//Region size: 8MB (0b10110)
		"ldr r0, =0x1800002D\n"
		"mcr p15, 0, r0, c6, c7, 0\n"
		
		//Set region 7 permissions:
		//Privileged: Read/write access
		//User: Read/write access
		"mrc p15, 0, r0, c5, c0, 2\n"
		"bic r0, r0, #(0b1111 << 28)\n"
		"orr r0, r0, #(0b0011 << 28)\n"
		"mcr p15, 0, r0, c5, c0, 2\n"
		
		: : : "r0"
	);	
	enable_MPU();
	enable_FIQ();
	enable_IRQ();
}

void jump_kernel()
{
	disable_IRQ();
	disable_FIQ();
	disable_MPU();
	
	//Get ARM11 out of the RAM
	int i;
	for (i = 0; i < 8; i++) {
		*(volatile u32*)(ARM11_EXCVEC_ADDR + i*4) = 0xeafffffe; //jump to itself
	}
	
	void (*entry)(void) = ZRELADDR;
	
	DEBUG("Jumping to kernel entry!");
	entry();
}

int main()
{
	backdoor(pwn_ARM11);
	backdoor(map_VRAM_MPU);

	ClearScreen();
	DEBUG("ARM9 linux loader by xerpi");	

	DEBUG("Loading the linux image...");
	Handle fd = FileOpen(LINUXIMAGE_FILENAME, OPEN_READ);
	
	DEBUG("FileOpen returned: %d", fd);
	
	if (fd == 0) {
		ERROR("Error opening the Linux image");
	}
	
	//Copy the kernel image code to FCRAM
	unsigned int n = 0, bin_size;
	bin_size = 0;
	while ((n = FileRead(fd, (void*)((u32)ZRELADDR+bin_size), 0x100000, bin_size)) > 0) {
		bin_size += n;
		DEBUG("Read: %d  bin_size: %d", n, bin_size);
	}
	FlushProcessDataCache(CURRENT_KPROCESS, (void *)ZRELADDR, bin_size);
	DEBUG("Loaded kernel image to %p, size 0x%X", ZRELADDR, bin_size);
	
	backdoor(jump_kernel);
	
	return 0;
}
