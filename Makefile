TARGET := 3DS_arm9_linux_loader

SOURCES = source
INCLUDES = include

C_FILES := $(foreach dir,$(SOURCES),$(wildcard $(dir)/*.c))
S_FILES := $(foreach dir,$(SOURCES),$(wildcard $(dir)/*.S))
OBJS	:= $(C_FILES:.c=.o) $(S_FILES:.S=.o)

PREFIX  = $(DEVKITARM)/bin/arm-none-eabi
CC      = $(PREFIX)-gcc
LD      = $(PREFIX)-ld
STRIP   = $(PREFIX)-strip
OBJCOPY = $(PREFIX)-objcopy
BIN2S   = $(DEVKITARM)/bin/bin2s

COMMON_FLAGS   = -marm -fomit-frame-pointer -mlittle-endian -fshort-wchar
ARM9_ASFLAGS   = $(COMMON_FLAGS) -mcpu=arm946e-s -march=armv5te
ARM11_ASFLAGS  = $(COMMON_FLAGS) -mcpu=mpcore    -march=armv6k
CFLAGS	= -Os -Wall $(ARM9_ASFLAGS) -I$(INCLUDES)
LDFLAGS = -nostartfiles -nostdlib
KEY     = 580006192800C5F0FBFB04E06A682088
IV      = 00000000000000000000000000000000

all: Launcher.dat

Launcher.dat: $(TARGET).bin
	python tools/3dsploit.py $(TARGET).bin Launcher_noGW.dat
	openssl enc -aes-128-cbc -K $(KEY) -iv $(IV) -in Launcher_noGW.dat -out Launcher.dat

$(TARGET).bin: $(TARGET).elf
	$(OBJCOPY) -S -O binary $(TARGET).elf $(TARGET).bin
$(TARGET).elf: $(OBJS)
	$(LD) $(LDFLAGS) -T linker.x $(OBJS) -o $(TARGET).elf

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@
%.o: %.S
	$(CC) $(ARM9_ASFLAGS) -c $< -o $@
source/arm11.o: source/arm11.S
	$(CC) $(ARM11_ASFLAGS) -c $^ -o $@

clean:
	@rm -rf $(OBJS) $(TARGET).elf $(TARGET).bin Launcher_noGW.dat Launcher.dat
	@echo "Cleaned!"
	
copy: Launcher.dat
	@cp Launcher_noGW.dat "/media/$(USER)/GATEWAYNAND/Launcher.dat"
	@sync
	@echo "Copied!"
