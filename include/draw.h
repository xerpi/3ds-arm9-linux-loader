#ifndef _DRAW_H_
#define _DRAW_H_

#include "3dstypes.h"

#define FCRAM	  (0x20000000)
#define FCRAM_END (0x28000000)
#define SCREEN_TOP_W  (400)
#define SCREEN_BOT_W  (340)
#define SCREEN_TOP_H  (240)
#define SCREEN_BOT_H  (240)

#define FB_TOP_SIZE	  (400*240*3)
#define FB_BOT_SIZE	  (340*240*3)

#define VRAM_BASE     (0x18000000)
#define FB_TOP_LEFT1  (VRAM_BASE)
#define FB_TOP_LEFT2  (FB_TOP_LEFT1  + FB_TOP_SIZE)
#define FB_TOP_RIGHT1 (FB_TOP_LEFT2  + FB_TOP_SIZE)
#define FB_TOP_RIGHT2 (FB_TOP_RIGHT1 + FB_TOP_SIZE)
#define FB_BOT_1      (FB_TOP_RIGHT2 + FB_TOP_SIZE)
#define FB_BOT_2      (FB_BOT_1      + FB_BOT_SIZE)

#define RED	   0xFF0000
#define GREEN  0x00FF00
#define BLUE   0x0000FF
#define CYAN   0x00FFFF
#define BLACK  0x000000
#define WHITE  0xFFFFFF

extern int console_y;

void ClearScreen();
void draw_plot(int x, int y, u32 color);
void draw_fillrect(int x, int y, int w, int h, u32 color);
//Return last X position
int font_draw_char(int x, int y, u32 color, char c);
int font_draw_string(int x, int y, u32 color, const char *string);
void font_draw_stringf(int x, int y, u32 color, const char *s, ...);
void console_printf(u32 color, const char *s, ...);

#define ERROR(args...) \
	do { \
		console_printf(RED, ## args); \
		while (1) ; \
	} while (0);
#define DEBUG(args...) console_printf(WHITE, ## args)



#endif
