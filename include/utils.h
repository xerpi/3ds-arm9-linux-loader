#ifndef UTILS_H
#define UTILS_H

#define CURRENT_KPROCESS 0xFFFF8001

extern void backdoor(void (*func)(void));

extern int FlushProcessDataCache(uint32_t process, void *addr, uint32_t size);

extern void disable_IRQ();
extern void enable_IRQ();
extern void disable_FIQ();
extern void enable_FIQ();
extern void disable_MPU();
extern void enable_MPU();

#endif
